# Wagashi Project
This project is a simple discount system with variable inputs calculated
dynamically for purchases. It's experimental so use with care.

A demo app is here: https://wagashi.herokuapp.com/purchases


<br><br><hr>
## Approaches
### break each models into very own
The idea is to break customers, products, discounts and purchases into 5
different tables and integrate them when necessary. It looks something like
this:

![Database structure](https://lh3.googleusercontent.com/5icUIBM5xRlNfdOsfNyRcbZzdWlsey-XLBDZ8AdPYj6Sknm640gEELPfL_irA6uOzZoCFU2cB0e0cnf767LnQib7-wYTrRjDGdJlVvgYegNvRs_sHLSbboCHLitcCKlW7S9WRCG1_vxCs4U8MqVJyDXN4QYzDhIR2SQOni65Rclp-P9BlfDQz1HlQuJDT7J-rQHPFvftxN2UegjODf5nx0tr4TE0nD97G4VYm3T_mGQtmW5P-WWr5YZqTT1z26sgKIzz45pGc_SX4lp0OGSkkOjr11yO90lsqZW76DOITFY7QT-scpxGN7DRWDG9dG7QZwf8YezwF9_PcI70jvJq_3e6QQYDKe653xZBNxqrCo2S5XnBdf3tNBjD2tcHW2JlczDcRsw3WsCTrgPKlWlvhnw52JCi1cK5VVIujClguwwUAcrQ5f9c0tYpXyjJFmy8EdmO7fLoqz7-EXIfsvX3stL2W-EhpxQOC6ZoMC8e2b12mWfbmpthTWDdgZFbRldYQ4Wrqhd4yBI4VqV6urk_uUoMYlaBwZ58WBodXPCmCegQnZU5eAYzqt9BBO0ZpO2Q9DKSJPS4K4aQi3XaI9vE6ynvxWVVagTM=w1850-h1194-no)

<br><br>
Using this approach allows more flexible data access over the database.
However, due to is fragmented nature, more query optimization efforts are
needed to acheive (n-1) query.
 
<br><br>
## Things to improve
To date, this experiment achieves the discount flexibility settings without
database query optimization. It can calculate the total price (and list them).
However, if the database grows to a size, the database optimization is needed.


<br><br>
## License
This project is under MIT License.
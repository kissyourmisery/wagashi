module DiscountsHelper

DISCOUNT_TYPE = { "price reduction (%)" => 0,
                  "free item reduction (units)" => 1
                }

DISCOUNT_STATUS = { "inactive" => 0,
                    "active" => 1
                  }


end

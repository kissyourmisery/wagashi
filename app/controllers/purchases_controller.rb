class PurchasesController < ApplicationController
  before_action :set_purchase, only: [:show, :destroy]

  def index
    @purchases = Purchase.all
  end


  def show
  end


  def new
    @purchase = Purchase.new
    @customers = Customer.all.map {|customer| [customer.name, customer.id]}
    @products = Product.all
    @discounts = Discount.all
  end


  def create
    @purchase = Purchase.create(purchase_params)
    @purchase.place_a_purchase(products_params_value)

    respond_to do |format|
      if @purchase.save
        format.html { redirect_to purchases_path, notice: 'purchase was successfully created.' }
        format.json { render :show, status: :created, location: @purchase }
      else
        format.html { render :index }
        format.json { render json: @purchase.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @purchase.destroy
    respond_to do |format|
      format.html { redirect_to purchases_url, notice: 'purchase was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    def set_purchase
      @purchase = Purchase.find_by(id: params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def purchase_params
      params.require(:purchase).permit(:customer_id)
    end

    def products_params_value
      params.require(:purchase).permit(products: [:id, :quantity])[:products]
    end
end
